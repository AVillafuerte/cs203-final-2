package WeAreNumber1;
import java.util.*;
public class LIFO <E> {
	private Stack <E> stack;
	public LIFO() {
		stack = new Stack<>();
	}
	public boolean empty() {
		return stack.empty();
	}
	public E peek() {
		return stack.peek();
	}
	public E pop() {
		return stack.pop();
	}
	public E push(E item) {
		return stack.push(item);
	}
	public int search(Object o) {
		return stack.search(o);
	}
}